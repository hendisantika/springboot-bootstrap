# Spring Boot with Bootstrap

## Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-bootstrap.git`
2. Go to your folder: `springboot-bootstrap`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: http://localhost:8080

### Index Page

![Index Page](img/index.png "Index Page")
